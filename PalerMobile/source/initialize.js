$(function() {
    MapsLib.initialize();
    
    $("#about").click(function() { 
      MapsLib.onExitMap(); 
      // localization
      if (Localize.locale != undefined) {
        Localize.about();
      }
      $("#listview").hide();
    });
    $("#search_page").click(function() { 
      MapsLib.onExitMap();
      // localization
      if (Localize.locale != undefined) {
        Localize.search_page();
      }
      $("#listview").hide();
    });
    $("#listview").click(function(){
      MapsLib.onExitMap();
      MapsLib.getListView();
      // localization
      if (Localize.locale != undefined) {
        Localize.list_page();
      }
      fixCSS();
      setTimeout(function(){
        $('span.ui-icon.ui-icon-arrow-r.ui-icon-shadow').each(function(){$(this).hide()});
      },1000);
    });
    $('#page-list').on(function(){ 
      fixCSS(); 
      if (Localize.locale != undefined) {
        Localize.list_page();
      }
    });

    $("#about_back").click(function() { 
      MapsLib.onEnterMap();
      $('#listview').show();
      resetDetailsPage( getLocalizedInitDetailsPage() ); 
    });
    $("#search_back").click(function() { 
      MapsLib.onEnterMap(); 
      $('#listview').show();
      resetDetailsPage( getLocalizedInitDetailsPage() ); 
    });
    $("#list_back").click(function() { 
      MapsLib.onEnterMap();
      $('#listview').show();
      resetDetailsPage( getLocalizedInitDetailsPage() );
    });
    $("#details_back").click(function() { 
      MapsLib.onEnterMap();
      $('#listview').show();
      resetDetailsPage( getLocalizedInitDetailsPage() );
    });

    $("#search").click(function(){
      MapsLib.onEnterMap();
      MapsLib.doSearch();
    });
    $("#search_top").click(function(){
      MapsLib.onEnterMap();
      MapsLib.doSearch();
    });
    $("#search_reset").click(function(){
      slideToggleAccoglienzaElements(true);
      slideToggleRistoroElements(true);
      slideToggleDivertimentoElements(true);
      slideToggleAperitivoElements(true);
      slideToggleCucinaElements(true);
      slideToggleConsolatoElements(true);
      slideToggleLuoghiDaVisitareElements(true);
      MapsLib.resetSearch();
    });
    window.addEventListener('popstate', function(e) { 
      MapsLib.onPopState();
    });
});
