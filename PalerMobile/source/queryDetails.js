/*
 * Copyleft BeardTeam, 2014
 * Released under GPLv3
 *
 * Author: Massimiliano Leone https://plus.google.com/+MassimilianoLeone
 *
 * addon for https://github.com/sfbrigade/Mobile-Fusion-Tables
 */
function queryDetailsCard(nome, previousLocation) {

  //   var queryCard = "https://www.google.com/fusiontables/embedviz?viz=CARD&q=select+*+from+"+PalerMobile.Global.fusionTableID+"+where+'nome'='"+nome+"'&tmplt=2&cpr=3\"";
  //   console.log("using queryCard:"+queryCard);

  function successCallback(responseData, textStatus, jqXHR) {
    try {
      var data = {};
      var columnsObject = responseData.columns;
      var contents = responseData.rows[0];
      var columns = Object.keys(columnsObject);
      for (var i=0;i<columns.length;i++) {
        var key = columnsObject[i].replace(".","#");
        var value = contents[i];
        data[key] = value;
      }

//       console.log(data);

      var html = "<div>";
      html += "<h3 class='infobox-header place-name' style='padding=0px 3px 0 3px;'>"+data.nome+"</h3>"; // nome
      html += "<div class='CSSTableGeneratorDetails'><table>";      
//       html += "<p class='infobox-subheader'>"; // start subheader
//       html += "<div class='details-tipo'>";

      // agenzia
      if (data['agenzia#intestazione']) {
//         console.log("agenzia");
//         console.log(data['agenzia#intestazione']);
        html += "<tr><td class='text-align-right'><span class='travel-agency'>"+"Agenzia di viaggi"+"</span></td></tr>";
      }

      if (data['consolato#console']) { // consolato section
        console.log("consolato");
        html += "<tr><td>c<br/></td></tr>";
        html += "<tr><td><b class='details-tipo-consulate'>Console: </b></td><td>"+data['consolato#console']+"</td></tr>";
        html += "<tr><td><b class='details-tipo-opening_hours'>Orari di apertura</i></td></tr>";
        html += "<tr><td><i class='details-tipo-days'>giorni: </i></td><td>"+data['consolato#orari#giorni']+"</td></tr>";
        var d = data['consolato#orari#apertura'];
        if (d!=NaN&&d!="NaN") {
          d = Number(d).toFixed(2);
        } else { d = ""; }
        html += "<tr><td><i class='details-tipo-begin_hour'>dalle</i></td><td>" +d+"</td></tr>";
        d = data['consolato#orari#chiusura'];
        if (d!=NaN&&d!="NaN") {
          d = Number(d).toFixed(2);
        } else { d = ""; }
        html += "<tr><td><i class='details-tipo-end-hour'>alle</i></td><td>"+d+"<//td></tr>";
        var note = data['consolato#orari#note'];
        if (note) {
          html += "<tr><td><b class='details-tipo-note'>note:</b></td><td>"+note+"</td></tr>";
        }
      } // end consolato

      var tipi_specifici = data['tipi-specifici'];
//       html += "<tr><td>"+tispi_specifici+"</td></tr>";

      // accoglienza section
      var stelle = data['accoglienza#stelle'];
      if (isNaN(stelle) == false) {
//         console.log("accoglienza");
        html += "<tr><td class='text-align-right'>"+tipi_specifici+"</td></tr>";
//         html += "<tr><td><b class='details-accomodate-category'>Categoria: </b></td><td><span>"+tipi_specifici+"</span></td></tr>";
        html += "<tr><td class='text-align-right'><b class='details-accomodate-stars'>Stelle</b></td><td><span>"+stelle+"</span></td></tr>";
        html += "<tr><td class='text-align-right'><b class='details-accomodate-rooms'>Camere</b></td><td><span>"+data['accoglienza#camere']+"</span></td></tr>";
        if (data['accoglienza#sale_meeting']) {
          html += "<tr><td class='text-align-right'><b class='details-accomodate-meeting'>Sale Meeting: </b></td><td><span>"+data['accoglienza#sale_meeting']+"</span></td></tr>";
        }
        if (data['accoglienza#residences']) {
          html += "<tr><td class='text-align-right'><b class='details-accomodate-residences'>Residences: </b></td><td><span>"+data['accoglienza#residences']+"</span></td></tr>";
        }
        if (data['accoglienza#manager']) {
          html += "<tr><td class='text-align-right'><b class='details-accomodate-director'>Direttore: </b></td><td><span>"+data['accoglienza#direttore']+"</span></td></tr>";
        }
        if (data['accoglienza#gestione']) {
          html += "<tr><td class='text-align-right'><b class='details-accomodate-management'>Gestione: </b></td><td><span>"+data['accoglienza#gestione']+"</span></td></tr>";
        }
        if (data['accoglienza#informazioni']) {
          html +="<tr><td class='text-align-right'><b class='details-accomodate-informations'>Informazioni: </b></td><td><span>"+data['accoglienza#informazioni']+"</span></td></tr>";
        }
      } // end accoglienza

      if (data.tipi.indexOf('ristoro') >= 0 || data.tipi.indexOf('divertimento') >= 0) { // ristoro/divertimento section
//         html += "<tr><td><br/></td></tr>";
        html += "<tr><td class='text-align-right'>"+tipi_specifici+"</td></tr>";
        if (data['divertimento-e-ristoro#cucina']) {
          html +="<tr><td class='text-align-right'><b class='details-cooking'>Cucina:</b></td><td><span> "+data['divertimento-e-ristoro#cucina']+"</span></td></tr>";
        }
        html += "<tr><td class='text-align-right'><b class='details-tipo-opening_hours'>Orari di apertura:</b></td></tr>";
        html += "<tr><td class='text-align-right'><i class='details-tipo-days'>giorni: </i></td><td><span>"+data['divertimento-e-ristoro#orari#giorni']+"</span></td></tr>";

        var d = data['divertimento-e-ristoro#orari#apertura'];
//         console.log(typeof d);
        if (d!=NaN&&d!="NaN") {
          d = Number(d).toFixed(2);
        } else { d = ""; }
        html += "<tr><td class='text-align-right'><i class='details-tipo-begin_hour'>dalle</i></td><td><span>"+d+"</span></td></tr>";
        d = data['divertimento-e-ristoro#orari#chiusura'];
        if (d!=NaN&&d!="NaN") {
          d = Number(d).toFixed(2);
        } else { d = ""; }
        html += "<tr><td class='text-align-right'><i class='details-tipo-end_hour'>alle</i></td><td><span>"+d+"</span></td></tr>";

        html += "<tr><td class='text-align-right'><i class='details-tipo-note'>note</i></td><td><span>"+data['divertimento-e-ristoro#orari#note']+"</span></td></tr>";
      } // end divertimento/ristoro

      if (data.tipi.indexOf('visitare') >= 0) {
        /*html +="<div class='details-visitare'>";
        html += tipi_specifici+"</i></td></tr>";*/

        html += "<tr><td class='text-align-right'>"+tipi_specifici+"</td></tr>";

        var orari_note = data['luogo-da-visitare#orari#note'];
        if (orari_note != undefined) {
          html +="<tr><td class='text-align-right'><i class='details-tipo-opening_hours'>Orari di apertura</i></td><td><span> "+orari_note+"</span></td></tr>";
        }
        var orari_servizi = data['luogo-da-visitare#orari#servizi'];
        if (orari_servizi != undefined) {
          html +="<tr><td class='text-align-right'><i class='services'>Servizi</i></td><td><span> "+orari_servizi +"</span></td></tr>";
        }
        var orari_visite = data['luogo-da-visitare#orari#visite'];
        if (orari_visite) {
          html += "<tr><td class='text-align-right'><i class='details-allowing-visit'>Visite</i></td><td><span> "+orari_visite+"</span></td></tr>";
        }
        var orari_prezzi = data['luogo-da-visitare#orari#prezzi'];
        if (orari_prezzi) {
          html += "<tr><td class='text-align-right'><i class='details-prices'>Prezzi</i></td><td><span> "+orari_prezzi+"</span></td></tr>";
        }

        var informazioni_storiche = data['luogo-da-visitare#informazioni-storiche'];
        if (informazioni_storiche) {
          html += "<tr><td class='text-align-right'><i class='details-historical_informations'>Informazioni storiche</i></td><td><span> "+informazioni_storiche+"</span></td></tr>";
        }
        var note = data['luogo-da-visitare#note'];
        if (note) {
          html += "<tr><td class='text-align-right'><i class='details-tipo-note'>Note</i></td><td><span> " +note+"</span></td></tr>";
        }
        var gestore_telefono = data['luogo-da-visitare#gestore#telefono'];
        if (gestore_telefono) {
          html += "<tr><td class='text-align-right'><i class='details-place-manager-phone'>Gestione</i></td><td><span> " +gestore_telefono+"</span></td></tr>";
        }
//         html += "</div>"; // end visitare
      }
//       html += "</div>"; // end details-tipo

      var telefono = data.telefono;
      var mobile = data.mobile;
      var email = data.email;
      var web = data.web;
      if (hasValue(telefono) || hasValue(mobile) || hasValue(email) || hasValue(web)) {
//         html += "<div class='details-contatti'><b class='details-contacts'>Contatti:</b>";
        html += "<tr><td><br/></td><td><br/></td></tr>";
        html += "<tr><td class='text-align-right'><b class='text-align-right'>Contatti</b></td><td><br/></td></tr>";

        if (hasValue(telefono) || hasValue(mobile)) {
//           html += "<div class='details-phone'>"; // start telefono/mobile
          //       console.log(telefono+" "+mobile);
          if (hasValue(telefono)) {
            html += "<tr><td class='text-align-right'><i class='details-landphone'>telefono</i></td><td><span> "+telefono+"</span></td></tr>";
          }
          if (hasValue(mobile)) {
            html += "<tr><td class='text-align-right'><i class='details-mobile'>mobile</i></td><td><span> "+mobile+"</span></td></tr>";
          }
//           html += "</div>"; // end details-phone
        }

        // start email/mobile    
        if (email || web) {
//           html += "<div class='details-internet'>";
          if (email) {
            html += "<tr><td class='text-align-right'><i class='details-email'>email</i></td><td><a href=mailto:"+email+">"+email+"</a></td></tr>";
          }
          if (web) {
            html += "<tr><td class='text-align-right'><i class='details-web'>web</i></td><td><a href="+web+">"+web+"</a></td></tr>";
          }
//           html += "</div>"; // end details-internet
        } // end email/mobile
//         html += "</div>"; // end details-contatti
      }

      // start address block
      var indirizzo = data.indirizzo;
      if (hasValue(indirizzo)) {
        //       console.log("indiriss: "+indirizzo);
//         html += "<div class='details-address'>";
        html += "<tr><td><br/></td><br/></tr>";
        html += "<tr><td class='text-align-right'><b class='details-address-prefix'>Indirizzo:</b></td><td><span>"+indirizzo+" "+data['numero-civico']+", " +data.citta+"</span>";
        if (data.quartiere.length > 1) {
          html +="<span class='details-district'>, quartiere</span><span> " +data.quartiere+"</span>";
        }
        html += "</td></tr>";        
//         html += "</div>"; // end address block
      }

//       html += "</p>" // end infobox-subheader
      html += "</table></div>" // end div tabella
      html += "</div>"; // div iniziale

      $('#details-content').html(html);

      if (PalerMobile.Global.language != undefined) { // translate
        var classes = Localization[Localize.locale].label.details_page.class;
        for (var cl in classes) {
          if ($('.'+cl).length > 0) {
            $('.'+cl).html(classes[cl]);
          }
        }

        setTimeout(function() {
          $('#list_back .ui-btn-inner .ui-btn-text').html(Localization[Localize.locale].button.details_page.back);
          $("h3.ui-title[role='heading'][aria-level='1']:not(:has(*))").html(Localization[Localize.locale].title.details_page.details);
        }, 150);
      }

//       console.log(".");

    } catch (e) {
      console.log(e);
      failureCallback(null, null, e);
    }
  }

  function failureCallback(jqXHR,textStatus, errorThrown) {
    if (jqXHR == null) {
      console.log(errorThrown);
      var html ="<div class='details-no_details' style='text-align:center;'>Spiacente, non &egrave; stato possibile ottenere ulteriori dettagli</div>";
      $('#details-content').html(html);
      setTimeout(function() {
        window.location.hash = previousLocation;
      }, 4000);
    } else if (jqXHR.status == 200 &&
      textStatus == 'OK') { // not error - workaround
        console.log("jqXHR:"+jqXHR);
    } else {
      var html ="<div class='details-no_details'>Spiacente, non &egrave; stato possibile ottenere ulteriori dettagli</div>";
      $('#details-content').html(html);
      setTimeout(function() {
        window.location.hash = previousLocation;
      }, 3000);

      resetDetailsPage(getLocalizedInitDetailsPage());
//       $('#details-content-text').html( details_html );
    }
  }

  function hasValue(value) {
    if (value == "" || value === "" || value === undefined || value == undefined || value == null || value === null || value.length === 0 || value.length == 0) {
      return false;
    }
    //     console.log("value: "+value+" returning true");
    return true;
  }
  
//   console.log(".");

  // webservice script url: https://script.google.com/macros/d/MoqSkcdItQkEw5tEIC49z0hNrVEl51b0O/edit?uiv=2&mid=ACjPJvEUHECrY0ReMnOvXp6yB4AaNFSFrQJhjO1C7mBkhDVbxsYoHonStDmqswtmqKR9HDvL5xKNKVrJ1ipTnLUkmm5P_QniCq81cfEtMbErpB7CG-E87tGx0mL38Wa-IFm0SQZ4mjSmVjE
  var encodedNome = encodeURIComponent(nome);
  //   console.log(encodedNome);
  // jsonp
  var queryPage = "https://script.google.com/macros/s/AKfycbyeSEK-1Xh1mkDZUsRjG1xKFamNhJQwAtyrQF4s620/dev?nome=" +encodedNome+"&callback?";
  // json
  //   var queryPage = "https://script.google.com/macros/s/AKfycbyeSEK-1Xh1mkDZUsRjG1xKFamNhJQwAtyrQF4s620/dev?nome="+encodedNome;

  // directly
  var url = "https://www.googleapis.com/fusiontables/v2/query?sql=SELECT * FROM " +PalerMobile.Global.fusionTableID +" where nome='"+encodedNome +"'&key="+PalerMobile.Global.googleApiKey;

//   console.log("using url:"+url);
//   console.log("using queryPage:" +queryPage);


  $.ajax({
    //       url: queryPage,
    url: url,
    async: true,
    //       dataType: 'jsonp',
    dataType: 'json',
    timeout: 7000,
    success: successCallback,
    error: failureCallback
      //   ,complete: completedCallback
  });

  return true;
}


function getLocalizedInitDetailsPage() {
  //   console.log(Localization[Localize.locale]);
  if (Localization[Localize.locale].message.details.data_loading != undefined)
    return Localization[Localize.locale].message.details.data_loading;
  else
    return "Caricamento dei dati...";
}

function resetDetailsPage(loadingHtmlText) {
  var lht;
  if (loadingHtmlText != undefined)
    lht = loadingHtmlText;
  else
    lht = "Caricamento dei dati...";

  //   $('#details-content-text').html( details_html );
  var detailsHtml = "<div align='center'><img src='source/images/ajax-loader.gif'><div id='details-content-text'>" +lht+"</div></div>"
  $('#details-content').html(detailsHtml);
}

/*
// Your Client ID can be retrieved from your project in the Google
// Developer Console, https://console.developers.google.com
var CLIENT_ID = '343159070149-56cg8i61tja0s3niuf5cbna3dqlap6v0.apps.googleusercontent.com';

var SCOPES = [
  'https://www.googleapis.com/auth/fusiontables'
];
// ,'https://www.googleapis.com/auth/drive'];

var authorized = false;

function handleClientLoad() {
  var timeout = 1000;
  setTimeout(function() {
    checkAuth();
  }, timeout);
}*/

//Check if current user has authorized this application.
/*function checkAuth() {
  //   console.log("checkAuth");
  gapi.auth.authorize({
    'client_id': CLIENT_ID,
    'scope': SCOPES.join(' '),
    'immediate': true
  }, handleAuthResult);
}*/

/**
 * Handle response from authorization server.
 *
 * @param {Object} authResult Authorization result.
 */
/*function handleAuthResult(authResult) {
  var authorizeDiv = document.getElementById(
    'authorize-div');
  //     console.log(authResult);
  if (authResult && !authResult.error) {
    // Hide auth UI, then load client library.
    authorizeDiv.style.display = 'none';
    //         callScriptFunction();
    authorized = true;
    console.log("authorized: " +
      authorized);
  } else {
    // Show auth UI, allowing the user to initiate authorization by
    // clicking authorize button.
    authorizeDiv.style.display =
      'inline';
    $('#authorize-button').click();
    console.log("...");
  }
}*/

/**
 * Initiate auth flow in response to user clicking authorize button.
 *
 * @param {Event} event Button click event.
 */
/*function handleAuthClick(event) {
  console.log('clicking');
  gapi.auth.authorize({
      client_id: CLIENT_ID,
      scope: SCOPES,
      immediate: false
    },
    handleAuthResult);
  return false;
}*/

/**
 * Append a pre element to the body containing the given message
 * as its text node.
 *
 * @param {string} message Text to be placed in pre element.
 */
/*function appendPre(message) {
  var pre = document.getElementById(
    'output');
  var textContent = document.createTextNode(
    message+'\n');
  pre.appendChild(textContent);
}*/