package net.iubris.palermobile_reloaded;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class MainActivity extends Activity {

	@SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		WebView webView = (WebView) findViewById(R.id.web_view);
		TextView textView = (TextView) findViewById(R.id.textviewNoConnection);
		if (hasConnectivity()) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
				WebView.setWebContentsDebuggingEnabled(true);
			}
			webView.getSettings().setJavaScriptEnabled(true);
			webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
			webView.setWebViewClient(new MyCustomWebViewClient());
			webView.loadUrl("http://palermobile.esy.es");
			webView.setVisibility(View.VISIBLE);
			textView.setVisibility(View.GONE);
		} else {
			webView.setVisibility(View.GONE);
			textView.setVisibility(View.VISIBLE);
			new Handler().postDelayed(new Runnable(){
				@Override
				public void run() {
					MainActivity.this.finish();					
			}}, 5000);
		}
		
		unlockScreen();
	}

	private class MyCustomWebViewClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}
	
	private boolean hasConnectivity() {
		ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
		 
		if (connectivityManager != null) {
			NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					NetworkInfo networkInfo = info[i];
					Log.d("MainActivity",""+networkInfo.getType()+" "+networkInfo.getState());
					if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
        return false;		
	}
	
	
	
	
	private void unlockScreen() {
//		if (Config.DEV) {
        Window window = this.getWindow();
        window.addFlags(LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(LayoutParams.FLAG_TURN_SCREEN_ON);
//        }
	}

}
