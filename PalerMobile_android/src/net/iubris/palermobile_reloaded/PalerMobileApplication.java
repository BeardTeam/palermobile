package net.iubris.palermobile_reloaded;

import java.io.File;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

public class PalerMobileApplication extends Application {

	private static final String PREFERENCES_FILE = "PALERMOBILE_PREFERENCES";
	private static final String APP_VERSION_PREFERENCE = "PALERMOBILE_APP_VERSION";
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		SharedPreferences sharedPreferences = getSharedPreferences(PREFERENCES_FILE, 0);
		int versionPrevious = sharedPreferences.getInt(APP_VERSION_PREFERENCE, 0);
		int versionActual = 0;
		try {
			versionActual = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
		} catch(NameNotFoundException e) {
		}
		if (versionPrevious>0) {
			clearApplicationData();
		}
		sharedPreferences.edit().putInt(APP_VERSION_PREFERENCE, versionActual).apply();
	}
	
	public void clearApplicationData() {
		File cache = getCacheDir();
		File appDir = new File(cache.getParent());
		if(appDir.exists()){
			String[] children = appDir.list();
			for(String s : children){
				if(!s.equals("lib")){
					deleteDir(new File(appDir, s));
					Log.i("TAG", "File /data/data/"+getPackageName()+"/" + s +" DELETED");
				}
			}
		}
	}	
	public static boolean deleteDir(File dir) {
	    if (dir != null && dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i = 0; i < children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }
	    return dir.delete();
	}
}
